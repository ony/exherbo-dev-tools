#!/usr/bin/env ruby
# vim: set sw=4 sts=4 et tw=80 :

# Copyright 2014 Alex Elsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# This is a script to, given a list of one or more installed
# packages, determine what their actual dependencies are via
# inspection of the DT_NEEDED entries of their installed ELF
# files.

# It requires dev-ruby/ruby-elf::arbor in order to parse
# said files, and uses dev-ruby/ruby-filemagic::arbor
# to get `file`-like output in order to mimic the output
# of the shell implementation of mscan.

require 'Paludis'
require 'getoptlong'

# dev-ruby/ruby-filemagic::arbor
require 'filemagic'
# dev-ruby/ruby-elf::arbor
require 'elf'

# Create a reverse index from files to their owners
# By doing this upfront, we can amortize the cost
# over every target we run over
def gen_contents_lookup( environment, root )
    # Initialize lookup such that new keys come with an empty
    # array as their value, allowing files to be appended without
    # needing to check for array presence.
    lookup = Hash.new {|h,k| h[k]=[]}

    packages = environment[
        Paludis::Selection::AllVersionsUnsorted.new(
            Paludis::FilteredGenerator.new(
                Paludis::Generator::All.new(),
                Paludis::Filter::InstalledAtRoot.new( root )
            )
        )
    ]

    packages.each do |pkgid|
        pkgid.contents and pkgid.contents.each do |file|
            if file.instance_of?( Paludis::ContentsFileEntry )
                file.each_metadata do |meta|
                    if meta.instance_of?( Paludis::MetadataFSPathKey )
                        lookup[meta.parse_value] << pkgid
                    end
                end
            end
        end
    end

    return lookup
end

# Take a list of dep spec strings, parse them, look up
# the matching package IDs, and flatten the result into
# a one-level list
def expand_user_targets( environment, root, targets )
    targets.map { |target_str|
        environment[
            # Use ...GroupedBySlot so the output is in the most
            # useful ordering in the case of wildcards and such
            Paludis::Selection::AllVersionsGroupedBySlot.new(
                Paludis::FilteredGenerator.new(
                    Paludis::Generator::Matches.new(
                       Paludis.parse_user_package_dep_spec(
                           target_str,
                           environment,
                           [ :allow_wildcards, :no_disambiguation ]
                       ),
                       nil,
                       []
                    ),
                    Paludis::Filter::InstalledAtRoot.new( root )
                )
            )
        ]
    }.flatten()
end

# Round up all the elves that are in a PackageID,
# grouped by their ELF class
def get_elf_contents( pkgid )
    elves = Hash.new {|h,k| h[k]=Hash.new}

    pkgid.contents.grep( Paludis::ContentsFileEntry ).each do |file|
        file.each_metadata do |meta|
            next unless meta.instance_of?( Paludis::MetadataFSPathKey )
            begin
                elf = Elf::File.open( meta.parse_value )
                elves[elf.elf_class][elf.path] = elf
            rescue Errno::ENOENT
                next
            rescue Elf::File::NotAnELF
                next
            end
        end
    end

    return elves
end

# Get the unique NEEDED libs of a list of ELF files
def get_all_linked_files( elves )
    linked = Hash.new {|h,k| h[k]=Hash.new}

    elves.each_pair do |elf_class, matching_files|
        matching_files.each_value do |elf|
            begin
                elf['.dynamic'].needed_libraries.each_pair do |soname, lib|
                    linked[lib.elf_class][lib.path] = true
                end
            rescue NoMethodError
                next
            ensure
                elf.close()
            end
        end
    end

    return linked
end

def get_owners_by_elfclass( contents_lookup, linked )
    owners = Hash.new {|h,k| h[k]=Hash.new{|h,k| h[k]=Hash.new}}

    linked.each_pair do |elf_class, matching_files|
        matching_files.keys.sort.each do |file|
            contents_lookup[file].each do |owner|
                owners[elf_class][owner][file] = true
            end
        end
    end

    return owners
end

# TODO: --ignore-set
# TODO: Maybe also a --root parameter at some point?
opts = GetoptLong.new(
    [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
)

opts.each do |opt, arg|
    case opt
    when '--help'
        puts "Usage: #{$0} target [ target... ]"
        exit 0
    end
end

if ARGV.length < 1
    puts "#{$0} requires at least one target. See #{$0} --help for usage information."
    exit 1
end

Paludis::Log.instance.log_level = Paludis::LogLevel::Warning
Paludis::Log.instance.program_name = $0

env = Paludis::PaludisEnvironment.new

contentsmap = gen_contents_lookup( env, '/' )

expand_user_targets( env, '/', ARGV ).each do |target|
    if not target.contents or target.contents.grep( Paludis::ContentsFileEntry ).empty?
        puts "#{target} has no contents of type file"
        next
    end

    elf_files = get_elf_contents( target )

    link_files = get_all_linked_files( elf_files )

    packages = get_owners_by_elfclass( contentsmap, link_files )

    [ Elf::Class::Elf64, Elf::Class::Elf32 ].each do |elf_class|
        puts "\e[32m#{elf_class} files:\e[00m"

        if elf_files[elf_class].empty?
            next
        end

        # `file` pads its output to be aligned for the longest filename
        # when run over more than one file at once.
        padto = elf_files[elf_class].keys.map { |x| x.length }.max + 1
        elf_files[elf_class].keys.sort { |a, b| a.to_s <=> b.to_s }.each do |file|
            padlen = padto - file.length
            puts "    #{file}:#{ ' ' * padlen }#{FileMagic.new.file( file )}"
        end

        if not packages[elf_class].empty?
            puts
        end

        packages[elf_class].keys.sort { |a, b| a.to_s <=> b.to_s }.each do |package|
            puts "\e[34;01m#{package}\e[00;00m"
            packages[elf_class][package].keys.sort.each do |reason|
                puts "    #{reason}"
            end
        end
    end
end

exit( 0 )

